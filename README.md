# My Personal dotfiles repo

This is a collection of my personal .dotfiles

## Usage

Clone this repo like this

```bash
git clone git@gitlab.com:christiaan.vandenpoel/dotfiles.git $HOME/.dotfiles
```

Then link the files you want

```bash
# oh-my-zsh
ln -s $HOME/.dotfiles/zshrc $HOME/.zshrc

# gitconfig
ln -s $HOME/.dotfiles/gitconfig $HOME/.gitconfig

# global ctags
ln -s $HOME/.dotfiles/ctags $HOME/.ctags

# global gitignore
ln -s $HOME/.dotfiles/gitignore $HOME/.gitignore
```

## Useful references

Some links to others sharing their dotfiles:

* https://github.com/statico/dotfiles

## Git aliases

### Nicer log

```bash
# nicer log
$ git lg

# and with changed files
$ git lg -p
```
